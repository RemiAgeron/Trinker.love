module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    allMale: function(p){
      return p.filter(index => index.gender == "Male")
    },

    allFemale: function(p){
      return p.filter(index => index.gender == "Female")
    },

    nbOfMale: function(p){
      return this.allMale(p).length
    },

    nbOfFemale: function(p){
      return this.allFemale(p).length
    },

    maleInterest: function(p){
      return p.filter(index => index.looking_for == "M")
    },

    nbOfMaleInterest: function(p){
      return this.maleInterest(p).length
    },

    femaleInterest: function(p){
      return p.filter(index => index.looking_for == "F")
    },

    nbOfFemaleInterest: function(p){
      return this.femaleInterest(p).length
    },

    nbOfMore2Kincome: function(p){
      return p.filter(index => parseFloat((index.income).substring(1)) > 2000).length
    },

    nbOfDramaFan: function(p){
      return p.filter(index => (index.pref_movie).includes("Drama")).length
    },

    nbOfFemaleSciFiFan: function(p){
      return this.allFemale(p).filter(index => (index.pref_movie).includes("Sci-Fi")).length
    },

    nbOfMinIncomeDocuFan: function(p){
      return p.filter(index => parseFloat((index.income).substring(1)) > 1482 && (index.pref_movie).includes("Documentary")).length
    },

    list4kIncome: function(p){
      return p
      .filter(index => parseFloat((index.income).substring(1)) > 4000)
      .map(index => "id: " + index.id + " " + index.first_name + " " + index.last_name)
    },

    richest: function(p){
      return (this.allMale(p)
      .sort((a, b) => parseFloat((b.income).substring(1)) - parseFloat((a.income).substring(1)))
      .map(name => name.first_name + " " + name.last_name + " id: " + name.id)
      )[0].yellow
    },

    averageIncome: function(p){
      total = 0
      p.forEach(e => total = parseFloat(total + parseFloat((e.income).substring(1))))
      return Math.round((total / p.length) * 100) / 100
    },

    medianIncome: function(p){
      p.sort((a,b) => parseFloat((b.income).substring(1)) - parseFloat((a.income).substring(1)))
      if(p.length % 2 == 0) return (((parseFloat((p[(p.length/2)+1].income).substring(1)) + parseFloat((p[p.length/2].income).substring(1)))/2) * 100) / 100
      return parseFloat((p[p.length/2].income).substring(1))
    },

    nbOfNorthHemisphere: function(p){
      return (p.filter(element => parseFloat(element.latitude) > 0)).length
    },

    averageIncomeInSouth: function(p){
      return this.averageIncome(p.filter(element => parseFloat(element.latitude) > 0))
    },

    closestList: function(p, idA, idB){
      tab = []
      userA = {
        latitude: parseFloat((((p.filter(e => e.id == idA))[0].latitude) * Math.PI) / 180), 
        longitude: parseFloat((((p.filter(e => e.id == idA))[0].longitude) * Math.PI) / 180)}
      if (idB != null) {
        userB = {
          latitude: parseFloat((((p.filter(e => e.id == idB))[0].latitude) * Math.PI) / 180), 
          longitude: parseFloat((((p.filter(e => e.id == idB))[0].longitude) * Math.PI) / 180)}
        d = Math.acos((Math.sin(userA.latitude) * Math.sin(userB.latitude)) + (Math.cos(userA.latitude) * Math.cos(userB.latitude) * Math.cos(userB.longitude - userA.longitude))) * 6378.137
        tab.push(d)
      } else {
        p.forEach(element => {
          userB = {
            latitude: parseFloat(((element.latitude) * Math.PI) / 180), 
            longitude: parseFloat(((element.longitude) * Math.PI) / 180)}
          d = Math.acos((Math.sin(userA.latitude) * Math.sin(userB.latitude)) + (Math.cos(userA.latitude) * Math.cos(userB.latitude) * Math.cos(userB.longitude - userA.longitude))) * 6378.137
          tab.push({first_name: element.first_name, last_name: element.last_name, id: element.id, distance: d})
        })
        tab.sort((a,b) => parseFloat(a.distance) - parseFloat(b.distance))
      }
      return tab
    },

    closest: function(p, id){
      return this.closestList(p, id)[1]
    },

    multipleClosest: function(p, id, nbr){
      return this.closestList(p, id).slice(1, nbr +1)
    },

    workAtGoogle: function(p){
      return p.filter(index => (index.email).includes("@google.")).map(e => "id: " + e.id + " " + e.first_name + " " + e.last_name)
    },

    eldest: function(p){
      target = (((p.map(e => new Date(e.date_of_birth))).sort((a,b) => a - b))[0])
      return (p.filter(element => (new Date(element.date_of_birth) + "") == target).map(e => e.first_name + " " + e.last_name + " born: " + e.date_of_birth))[0].yellow
    },

    youngest: function(p){
      target = (((p.map(e => new Date(e.date_of_birth))).sort((a,b) => b - a))[0])
      return (p.filter(element => (new Date(element.date_of_birth) + "") == target).map(e => e.first_name + " " + e.last_name + " born: " + e.date_of_birth))[0].yellow
    },

    averageAge: function(p){
      total = 0
      p.forEach(element => total += Math.abs((new Date(Date.now() - (new Date(element.date_of_birth)).getTime())).getUTCFullYear() - 1970))
      return parseInt(total / p.length)
    },

    CatList: function(p){
      allTabCat = []; tabCat = [];
      p.forEach(element => {
        if(element.pref_movie.includes("|")){
          split = element.pref_movie.split("|")
          split.forEach(e => allTabCat.push({name: e, id: element.id}))
        } else if (element.pref_movie != "(no genres listed)") {
          allTabCat.push({name: element.pref_movie, id: element.id})
        }
      })
      allTabCat.forEach(element => {
        if ((tabCat.filter(e => e.name == element.name)).length == 0) {
          tabCat.push({name: element.name, fan: [element.id]})
        } else {
          for (i = 0; i < tabCat.length; i++) {
            if (tabCat[i].name == element.name) {
              tabCat[i].fan.push(element.id)
            }
          }
        }
      })
      return tabCat.sort((a,b) => b.fan.length - a.fan.length)
    },

    mostPopularCat: function(p){
      return (this.CatList(p).map(e => e.name)[0]).green
    },

    mostPopularCatList: function(p){
      return this.CatList(p).map(e => e.name)
    },

    listOfCatAndFan: function(p){
      return this.CatList(p).map(e => e.name + " fan: " + e.fan.length)
    },

    averageAgeMaleBaWMovies: function(p){
      return String(this.averageAge(
        this.allMale(p).filter(element => element.pref_movie.includes("Film-Noir"))
      )).yellow
    },

    averageAgeFemaleBaWMovies: function(p){
      return String(this.averageAge(
        this.allFemale(p)
        .filter(element => element.pref_movie.includes("Film-Noir"))
        .filter(element => parseFloat(element.longitude) > parseInt(-5))
        .filter(element => parseFloat(element.longitude) < parseInt(15))
        .filter(element => parseFloat((element.income).substring(1)) < parseFloat(this.averageIncome(this.allMale(p))))
      )).yellow
    },

    couplePrefMovies: function(p, aG, aI){
      aI = aI == "F" ? this.femaleInterest(p) : this.maleInterest(p)
      let userA = aG == "Male" ? this.allMale(aI) : this.allFemale(aI)
      let match = []
      bI = aG == "Female" ? this.femaleInterest(p) : this.maleInterest(p)
      let userB = aI == "M" ? this.allMale(bI) : this.allFemale(bI)
      userA.forEach(a => {
        userCat = []
        if(a.pref_movie.includes("|")){
          a.pref_movie.split("|").forEach(e => userCat.push(e))
        } else if (a.pref_movie != "(no genres listed)") userCat.push(a.pref_movie)
        userCat.forEach(c => {
          userB.forEach(b => {
            if(b.pref_movie.includes(c)) match.push({a: a,b: b})
            // if(b.pref_movie.includes(c)) match.push(a.first_name + " " + a.last_name + " & " + b.first_name + " " + b.last_name)
            // if(b.pref_movie.includes(c)) match.push(a.pref_movie + " & " + b.pref_movie)
            // if(b.pref_movie.includes(c)) match.push(a.gender + " & " + b.gender)
            // if(b.pref_movie.includes(c)) match.push(a.looking_for + " & " + b.looking_for)
          })
        })
      })
      return match
    },

    maleWantMaleNear: function(p, aG, aI){
      let tab = []
      this.couplePrefMovies(p, aG, aI).forEach(element => tab.push({
        userA: element.a, 
        userB: element.b, 
        d: this.closestList(p, (element.a).id, (element.b).id)[0]
      }))
      return tab.sort((a,b) => a.d - b.d).map(e => e.userA.first_name + " " + e.userA.last_name + " & " + e.userB.first_name + " " + e.userB.last_name + " at: " + Math.round(e.d * 100) / 100 + "Km")[0].yellow
    },

    femaleWantMale: function(p, aG, aI){
      return this.couplePrefMovies(p, aG, aI).map(e => e.a.first_name + " " + e.a.last_name + " & " + e.b.first_name + " " + e.b.last_name)
    },

    match: function(p){
      return " Not working for now ".bgBlack.white
    }
}